/**
 *   This file is part of wald:find - a library for querying RDF.
 *   Copyright (C) 2016  Kuno Woudt <kuno@frob.nl>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of copyleft-next 0.3.1.  See copyleft-next-0.3.1.txt.
 */

'use strict';

const _s = require('underscore.string');
const fs = require('fs');
const when = require('when');

function loadFile(iri) {
    const _iri = _s(iri);
    if (_iri.startsWith('http://') || _iri.startsWith('http://')) {
        // remote iri, fall through
    } else if (_iri.startsWith('/')) {
        // local absolute path
        const input = fs.readFileSync(iri);
        return when(input);
    } else {
        // local relative path
        const input = fs.readFileSync(__dirname + '/' + iri);
        return when(input);
    }
}

module.exports = loadFile;
