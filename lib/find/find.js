/**
 *   This file is part of wald:find - a library for querying RDF.
 *   Copyright (C) 2016  Kuno Woudt <kuno@frob.nl>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of copyleft-next 0.3.1.  See copyleft-next-0.3.1.txt.
 */

'use strict';

const namespace = require('./namespace');
const query = require('./query');
const tools = require('./tools');

function firstValues(obj) {
    var ret = {};
    for (var p in obj) {
        if (obj.hasOwnProperty(p) && obj[p].length > 0) {
            ret[p] = obj[p][0];
        } else {
            ret[p] = obj[p];
        }
    }

    return ret;
}

module.exports = {
    a: namespace.namespaces.rdf.type,
    factory: query.factory,
    firstValues: firstValues,
    init: tools.init,
    loadPrefixes: namespace.loadPrefixes,
    namespaces: namespace.namespaces,
    prefix: namespace.prefix,
    qname: namespace.qname,
    Query: query.Query,
    shortenKeys: namespace.shortenKeys,
    tools: tools,
};
