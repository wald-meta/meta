/**
 *   This file is part of wald:data - the storage back-end of wald:meta.
 *   Copyright (C) 2016  Kuno Woudt <kuno@frob.nl>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of copyleft-next 0.3.1.  See copyleft-next-0.3.1.txt.
 */

'use strict';

const edit = require('./edit');
const find = require('wald-find');
const fs = require('fs');
const httpinvoke = require('httpinvoke');
const when = require('when');

const configFile = process.env.WALD_DATA_CONFIG_FILE
    ? process.env.WALD_DATA_CONFIG_FILE
    : __dirname + '/../test/test-config.ttl';

const datasetPath = process.env.WALD_DATA_DATASET_PATH
    ? process.env.WALD_DATA_DATASET_PATH
    : __dirname + '/../test/store/';

console.log('Using configuration: ' + configFile);
console.log('Using dataset path:  ' + datasetPath);

const configBody = fs.readFileSync(configFile, 'UTF-8');

function verifyDatasetExists(dataset) {
    // FIXME: use superagent?
    return when(
        httpinvoke('http://localhost:3030/$/datasets/' + dataset, 'GET')
    ).then(data => {
        // 200 OK if the dataset exists
        // 404 Not Found if it does not
        if (data.statusCode !== 200) {
            throw new Error('dataset "' + dataset + '" does not exist');
        }
    });
}

function createDataset(cfg, dataset) {
    return edit.fusekiConfiguration(cfg, dataset, datasetPath).then(turtle => {
        return when(
            httpinvoke('http://localhost:3030/$/datasets/', 'POST', {
                headers: { 'Content-Type': 'text/turtle' },
                input: turtle,
            })
        );
    });
}

function initDataset(cfg, dataset) {
    return verifyDatasetExists(dataset)
        .then(() => {
            console.log('Dataset "' + dataset + '" verified.');
        })
        .catch(err => {
            return createDataset(cfg, dataset)
                .then(() => {
                    console.log('Dataset "' + dataset + '" created.');
                })
                .catch(err => {
                    console.log(err);
                });
        });
}

find.tools.parseTurtle(configBody).then(function(datastore) {
    const cfg = edit.entityConfiguration(datastore);

    initDataset(cfg, cfg.dataset);
    initDataset(cfg, 'meta');
});
